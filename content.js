const blockAds = () => {
    const adElements = document.querySelectorAll('.ad-container, .video-ads, .ytp-ad-module, [id^="google_ads_iframe"]');

    adElements.forEach((adElement) => {
        adElement.remove();
    });
};

const removeAnnotations = () => {
    const annotationElements = document.querySelectorAll('.annotation, .ytp-ce-element');

    annotationElements.forEach((annotationElement) => {
        annotationElement.remove();
    });
};

const observer = new MutationObserver(() => {
    blockAds();
    removeAnnotations();
});

observer.observe(document.body, {
    childList: true,
    subtree: true,
});
